import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import axios from 'axios';
import VuePaginate from 'vue-paginate'

// Estilos 
import './styles.scss'
import 'buefy/lib/buefy.css'
import 'font-awesome/scss/font-awesome.scss'

Vue.use(Buefy, { defaultIconPack: 'fa' })
Vue.use(VuePaginate)

Vue.prototype.$http = axios

new Vue({
  el: '#app',
  render: h => h(App)
})
